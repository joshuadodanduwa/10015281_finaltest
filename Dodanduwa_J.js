//Creating a class to hold the properties of a van
class Van
{
    //Creating a constructor to create the properties of the van
    constructor(vanID, capacity, license, costPerDay, insurance)
    {
        this.VanID = vanID
        this.Capacity = capacity
        this.License = license
        this.CostPerDay = costPerDay
        this.Insurance = insurance
    }

    //Getters and Setters allow values to be passed from the user's input to TotalCost method (just the hire Cost and insurance) and the output (console browser)
    get VanID() {return this._vanID}
    set VanID(value) {this._vanID = value}

    get Capacity() {return this._capacity}
    set Capacity(value) {this._capacity = value}
    
    get License() {return this._license}
    set License(value) {this._license = value}

    get CostPerDay() {return this._costPerDay}
    set CostPerDay(value) {this._costPerDay = value}

    get Insurance() {return this._insurance}
    set Insurance(value) {this._insurance = value}

    //This method will calculate the total cost of hire by adding the hire cost and insurance together
    TotalCost() 
    {
        return this._costPerDay + this._insurance
    }
}

//Creating a new object that will allow user to input the properties of the van they are hiring
VanHire = new Van()

//Asking the user to input the properties of the van they are hiring (the Number in front of the prompt makes sure that property is a number) 
VanHire.VanID = prompt("Welcome to EZ Van Hire. Please enter a van ID:")
VanHire.Capacity = Number(prompt("Please enter the load capacity (m^3):"))
VanHire.License = prompt("Please enter the license type:")
VanHire.CostPerDay = Number(prompt("Please enter the cost of hire per day ($):"))
VanHire.Insurance = Number(prompt("Please Enter the cost of insurance per day ($):"))


//Outputting properties and total cost of hire with insurance (the toFixed(2) rounds that property to 2 decimal places)
console.log("EZ Van Hire: Van Details")
console.log("------------------------")
console.log(`Van ID                     : ${VanHire.VanID}`)
console.log(`Load Capacity (m^3)        : ${(VanHire.Capacity)}`)
console.log(`License Type               : ${VanHire.License}`)
console.log(`Cost of Hire per Day       : $${(VanHire.CostPerDay).toFixed(2)}`)
console.log(`Cost of Insurance per Day  : $${(VanHire.Insurance).toFixed(2)}`)
console.log(`Hire Cost Total            : $${(VanHire.TotalCost()).toFixed(2)}`)